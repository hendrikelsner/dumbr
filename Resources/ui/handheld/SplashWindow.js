function SplashWindow(options) {
	//create component instance
	var self = Ti.UI.createWindow({
		backgroundColor: '#000',
		orientationModes: [Titanium.UI.PORTRAIT],
		navBarHidden: false,
		fullscreen: false,
		statusBarStyle: Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT
	});
	
	//construct UI
	var imageView = Ti.UI.createView({
		backgroundImage: '/images/DumbrLadeScreen.jpg',
		center: {x: '50%', y:'50%'},
		width: Ti.UI.FILL,
		height: Ti.UI.FILL
	});
	self.add(imageView);
	
	// Activity Indicator
	var activityIndicator = Ti.UI.createActivityIndicator({
		color: 'white',
		style: Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN,
		center: {x: '50%', y: '65%'},
		height:Ti.UI.SIZE,
		width:Ti.UI.SIZE
	});
	imageView.add(activityIndicator);
	activityIndicator.show();
	
	lblVersion = Ti.UI.createLabel({
		text: 'Version 1.2',
		color: '#ffffff',
		font: {fontSize: 12},
		bottom: 0,
		left:0,
		width: Ti.UI.SIZE,
		height: Ti.UI.SIZE
	});
	imageView.add(lblVersion);
	
	return self;
}

//make constructor function the public component interface
module.exports = SplashWindow;