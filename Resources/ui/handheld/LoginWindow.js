function LoginWindow(options) {
	//create component instance
	var self = Ti.UI.createWindow({
		backgroundColor: '#000',
		orientationModes: [Titanium.UI.PORTRAIT],
		navBarHidden: false,
		fullscreen: false,
		statusBarStyle: Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT
	});
	
	var fieldLeft = 10;
	var fieldHeight = 40;
	var fieldWidth = 280;
	var fieldBackgroundColor = '#b4b2b2';
	var fieldTextColor = '#ffffff';
	var fieldDistance = 20;
	var iconRight = 10;
	var iconWidth = fieldHeight/1.5;
	
	function createUser(username, pwd) {
		Ti.App.Properties.setString(username, pwd);
	}
	createUser('admin', '1234');
	
	var backgroundImage = Ti.UI.createImageView({
		top: 0,
		left: 0,
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		image: '/images/Login.png',
	});
	self.add(backgroundImage);
	
	// LOGIN
	var loginBackground = Ti.UI.createView({
		center: {x: '50%'},
		top: 160,
		width: fieldWidth,
		height: fieldHeight,
		backgroundColor: fieldBackgroundColor
	});

	var loginField = Ti.UI.createTextField({
		center: {y: '50%'},
		left: fieldLeft,
		height: fieldHeight - 10,
		width: fieldWidth - iconWidth,
		hintText: 'Benutzername',
		backgroundColor: 'transparent',
		tintColor: fieldTextColor,
		color: fieldTextColor,
		autocapitalization: false,
		autocorrect: false,
		returnKeyType: Ti.UI.RETURNKEY_NEXT
	});
	loginField.addEventListener('return', function () {
		passwordField.focus();
	});
	loginBackground.add(loginField);
	var loginIcon = Ti.UI.createImageView({
		right: iconRight,
		center: {y: '50%'},
		image: '/images/Freund_Button.png',
		baseImage: '/images/Freund_Button.png',
		failImage: '/images/Freund_Button_False.png',
		height: 'auto',
		width: iconWidth
	});
	loginBackground.add(loginIcon);
	self.add(loginBackground);
	
	// PASSWORD
	var passwordBackground = Ti.UI.createView({
		center: {x: '50%'},
		top: loginBackground.top + loginBackground.height + fieldDistance,
		width: fieldWidth,
		height: fieldHeight,
		backgroundColor: fieldBackgroundColor
	});
	var passwordField = Ti.UI.createTextField({
		center: {y: '50%'},
		left: fieldLeft,
		height: fieldHeight - 10,
		width: fieldWidth - iconWidth,
		hintText: 'Passwort',
		backgroundColor: 'transparent',
		tintColor: fieldTextColor,
		color: fieldTextColor,
		autocapitalization: false,
		autocorrect: false,
		passwordMask: true,
		clearOnEdit: true,
		returnKeyType: Ti.UI.RETURNKEY_JOIN
	});
	passwordField.addEventListener('return', function () {
		if (Ti.App.Properties.hasProperty(loginField.value)) {
			loginIcon.image = loginIcon.baseImage;
			if (Ti.App.Properties.getString(loginField.value) == passwordField.value) {
				openMainWindow(loginField.value);
				passwordField.value = '';
				passwordIcon.image = passwordIcon.baseImage;
			} else {
				passwordIcon.image = passwordIcon.failImage;
			}
		} else {
			loginIcon.image = loginIcon.failImage;
			loginField.value = '';
			passwordField.value = '';
		}
	});
	passwordBackground.add(passwordField);
	var passwordIcon = Ti.UI.createImageView({
		right: iconRight,
		center: {y: '50%'},
		image: '/images/PasswortKey.png',
		baseImage: '/images/PasswortKey.png',
		failImage: '/images/PasswortKey_False.png',
		height: 'auto',
		width: iconWidth
	});
	passwordBackground.add(passwordIcon);
	self.add(passwordBackground);
	
	var facebookLogin = Ti.UI.createImageView({
		top: passwordBackground.top + passwordBackground.height + fieldDistance*2,
		center: {x: '50%'},
		image: '/images/Facebooklogin.png',
		width: fieldWidth,
		height: 'auto'
	});
	facebookLogin.addEventListener('singletap', function () {
		openMainWindow();
	});
	self.add(facebookLogin);
	
	backgroundImage.addEventListener('singletap', function () {
		passwordField.blur();
		loginField.blur();
	});
	
	
	function openMainWindow (user) {
		var Window = require('ui/handheld/ApplicationWindow');
		var window = new Window(user).open({transition: Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT});
	}
	
	
	return self;
}

//make constructor function the public component interface
module.exports = LoginWindow;