//Application Window Component Constructor
function ApplicationWindow(user) {
	//load component dependencies
	var FirstView = require('ui/common/FirstView');
		
	//create component instance
	var self = Ti.UI.createWindow({
		backgroundColor:'#ffffff',
		orientationModes: [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
		navBarHidden: false,
		fullscreen: false,
		statusBarStyle: Titanium.UI.iPhone.StatusBar.LIGHT_CONTENT
	});
		
	//construct UI
	var firstView = new FirstView(self, user);
	self.add(firstView);
	
	return self;
}

//make constructor function the public component interface
module.exports = ApplicationWindow;
