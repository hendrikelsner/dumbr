//FirstView Component Constructor
function FirstView(parentWindow, user) {

	var self = Ti.UI.createView({
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		backgroundColor: '#f5f5f7'
	});
	
	//PHOTO FUNCTIONS
	var ImageFactory = require('ti.imagefactory');
	
	var photoFolder = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'Fotos');
	if (!photoFolder.exists()) {
		photoFolder.createDirectory();
	}
	function getImages(folder) {
		var files = photoFolder.getDirectoryListing();
		var tempFiles = [];
		for ( var i = 0; i<files.length;  i++) {
			if (files[i].substr(-4, 4) == '.jpg') {
				tempFiles.unshift(files[i]);
			}
		}
		return tempFiles;
	}
	var files = getImages(photoFolder);
	
	var fb = require('facebook');
	function loginToFB() {
		// login to facebook with standart dialog
		fb.appid = '1873961802742825';
		fb.permissions = ['publish_stream'];
		fb.forceDialogAuth = true;
		fb.addEventListener('login', function(e) {
		    if (e.success) {
		        alert('Du hast dich erfolgreich eingeloggt.');
		    } else if (e.error) {
		        alert('Dein Login konnte nicht ausgeführt werden.');
		    } else if (e.cancelled) {
		        alert('Du musst dich einloggen, um ein Foto auf Facebook teilen zu können.');
		    }
		});
		fb.authorize();
	}
	
	function postToFB(args) {
		// post to facebook (args.text :: String), (args.photo :: Ti.Filesystem.File)
		// Check for login status and open login dialog if needed
		if (fb.loggedIn) {
			activityIndicator.show();
			var imgBlob = args.photo.read();
			var postData = {
				message: 'Geteilt mit Dumbr.',
				picture: imgBlob
			};
			fb.requestWithGraphPath('me/photos', postData, 'POST', function(e) {
				if (e.success && e.result) {
					alert('Das Foto wurde geteilt.');
					activityIndicator.hide()
				} else {
					if (e.error) {
						alert('Das Foto konnte nicht geteilt werden.');
						activityIndicator.hide()
					}
				}
			});
		} else {
			loginToFB();
		}
	}
	
	var menuBarHeight = 81/2;
	
	//header image
	var headerBackground = Ti.UI.createView({
		backgroundColor: '#f5f5f7',
		top: 0,
		left: 0,
		width: Ti.UI.FILL,
		height: 117/2
	})
	self.add(headerBackground);
	
	var headerImage = Ti.UI.createImageView({
		image: '/images/Dumbr.png',
		center: {x: '50%', y:'50%'},
		width: Ti.UI.FILL,
		height: 117/2
	});
	headerBackground.add(headerImage);
	
	var btnLogout = Ti.UI.createButton({
		backgroundImage: '/images/Logout.png',
		width: 40/2,
		height: 46/2,
		center: {y: '60%'},
		right: 10
	});
	btnLogout.addEventListener('singletap', function () {
		parentWindow.close({transition: Titanium.UI.iPhone.AnimationStyle.FLIP_FROM_RIGHT});
	});
	headerBackground.add(btnLogout);
	
	//tabbar
	var tabBar = Ti.UI.createView({
		backgroundColor: '#bbbbbc',
		top: headerBackground.height,
		left: 0,
		width: Ti.UI.FILL,
		height: 156/2,
	});
	self.add(tabBar);
	
	var btnContainer = Ti.UI.createView({
		height: tabBar.height,
		width: Ti.UI.SIZE,
		center: {x: '50%'}
	});
	tabBar.add(btnContainer);
	
	
	var btnPhoto = Ti.UI.createView({
		backgroundImage: '/images/MacheinFoto_Ausgewählt.png',
		backgroundSelected: '/images/MacheinFoto_Ausgewählt.png',
		backgroundDeselected: '/images/MacheinFoto_Nicht_Ausgewählt.png',
		top: 0,
		left: 0,
		width: 298/2,
		height: tabBar.height
	});
	btnContainer.add(btnPhoto);
	btnPhoto.addEventListener('singletap', function (e) {// @TODO animated change
		if (!menuBar.extended) {
			btnPhoto.backgroundImage = btnPhoto.backgroundSelected;
			btnFavorite.backgroundImage = btnFavorite.backgroundDeselected;
			favoritesView.animate({right: -1000, duration: 300});
			takePictureView.animate({left: 0, duration: 300});
		}
	});
	
	var separator = Ti.UI.createView({
		left: btnPhoto.width,
		height: tabBar.height,
		width: 1,
		backgroundColor: '#ee654d'
	});
	btnContainer.add(separator);
	
	var btnFavorite = Ti.UI.createView({
		backgroundImage: '/images/Favoriten_Nicht_Ausgewählt.png',
		backgroundSelected: '/images/Favoriten_Ausgewählt.png',
		backgroundDeselected: '/images/Favoriten_Nicht_Ausgewählt.png',
		top: 0,
		left: btnPhoto.width+1,
		width: 298/2,
		height: tabBar.height
	});
	btnContainer.add(btnFavorite);
	btnFavorite.addEventListener('singletap', function (e) {
		if (!menuBar.extended) {
			btnFavorite.backgroundImage = btnFavorite.backgroundSelected;
			btnPhoto.backgroundImage = btnPhoto.backgroundDeselected;
			favoritesView.animate({right: 0, duration: 300});
			takePictureView.animate({left: -1000, duration: 300});
		}
	});
	
	var userIcon = Ti.UI.createImageView({
		image: '/images/Benutzer_pic.png',
		height: 210/4,
		width: 164/4,
		left: 0,
		top: 35
	});
	self.add(userIcon);
	
	//content-area
	var contentArea = Ti.UI.createView({
		top: tabBar.height + tabBar.top,
		backgroundColor: '#77767b',
		center: {x: '50%'},
		left: 0,
		width: Ti.UI.FILL,
		height: Ti.UI.FILL,
		bottom: menuBarHeight
	});
	self.add(contentArea);
	
	var contentBackgroundImage = Ti.UI.createView({
		backgroundImage: '/images/Hintergrund.png',
		width: Ti.UI.FILL,
		height: Ti.UI.FILL
	});
	contentArea.add(contentBackgroundImage);
	
	// Activity Indicator
	var activityIndicator = Ti.UI.createActivityIndicator({
		color: 'white',
		style: Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN,
		center: {x: '48%', y: '50%'},
		height: Ti.UI.SIZE,
		width: Ti.UI.SIZE
	});
	contentArea.add(activityIndicator);
	
	takePictureView = Ti.UI.createView({
		top: 0,
		left:0,
		width: 320,
		height: 302,
		zIndex: 1
	});
	contentArea.add(takePictureView);
	
	var btnTakePicture = Ti.UI.createButton({
		center: {y: '50%', x: ((files.length > 0) ? '30%' : '50%')},
		backgroundImage: '/images/Kamera_Button.png',
		width: 220/2,
		height: 213/2,
		zIndex: 2
	});
	takePictureView.add(btnTakePicture);
	
	btnTakePicture.addEventListener('singletap', function (e) {
		Ti.Media.showCamera({
	        showControls: true,
	        mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
	        autohide: true, 
	        allowEditing: false,
	        success: function(event) {
	        	activityIndicator.show();
	        	var tmpFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'camera_photo.png');
	        	tmpFile.write(event.media);
		        Ti.App.fireEvent('saveFile', {path: tmpFile.nativePath});
	        },
	        cancel:function() {
	        	alert('cancel');
	        },
	        error:function(error) {
	        	alert('error '+error);
	        }
		});
	});
	
	function writeFile(path) {
		var photoFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, 'Fotos', ('camera_photo'+files.length.toString()+'.jpg'));
		var tmpFileBlob = Titanium.Filesystem.getFile(path).toBlob();
		var tmpFileResized = tmpFileBlob.imageAsResized( (tmpFileBlob.width/8), (tmpFileBlob.height/8) );
		tmpFileResized = ImageFactory.compress(tmpFileResized, 0.5);// @TODO Image Compression
	    photoFile.write(tmpFileResized);
		btnTakePicture.center = {y: '50%', x: '30%'};
		scrollPhotos.visible = true;
		updatePreviews();
		activityIndicator.hide();
	}
	Ti.App.addEventListener('saveFile', function (args) {
		writeFile(args.path)
	});
	
	var scrollPhotos = Ti.UI.createScrollView({
		center: {x: '70%'},
		width: btnTakePicture.width,
		height: Ti.UI.FILL,
		//backgroundColor: '#88000000',
		layout: 'vertical',
		horizontalWrap: true,
		visible: (files.length > 0),
	});
	takePictureView.add(scrollPhotos);
	
	// generate Photo previews
	function updatePreviews() {
		files = getImages(photoFolder);
		scrollPhotos.removeAllChildren();
		btnTakePicture.center = {y: '50%', x: ((files.length > 0) ? '30%' : '50%')};
		for (var i = 0; i < files.length; i++) {
			var imageFile = Ti.Filesystem.getFile(photoFolder.nativePath, files[i]);
			
			var btnDelete = Ti.UI.createView({
				bottom: 0,
				right: 0,
				width: scrollPhotos.width/2.5,
				height: scrollPhotos.width/2.5,
				borderRadius: scrollPhotos.width/5,
				backgroundColor: 'red',
				opacity: 0.0,
				imageFile: imageFile,
				title: 'btnDelete',
			});
			btnDelete.addEventListener('click', function(e) {
				var imageFile = e.source.imageFile;
				imageFile.deleteFile();
				updatePreviews();
			})
			var lblDelete = Ti.UI.createLabel({
				text: 'X',
				width: Ti.UI.SIZE,
				height: Ti.UI.SIZE,
				center: {x: '50%', y:'50%'},
				color: '#ffffff',
				font: {fontSize: 16}
			});
			btnDelete.add(lblDelete);
			
			var btnShare = Ti.UI.createView({
				top: 0,
				right: 0,
				width: scrollPhotos.width/2.5,
				height: scrollPhotos.width/2.5,
				borderRadius: scrollPhotos.width/5,
				backgroundColor: '#3c609e',
				opacity: 0.0,
				imageFile: imageFile
			});
			btnShare.addEventListener('click', function(e) {
				postToFB({photo: e.source.imageFile});
			})
			var lblShare = Ti.UI.createLabel({
				text: 'f',
				width: Ti.UI.SIZE,
				height: Ti.UI.SIZE,
				center: {x: '50%', y:'50%'},
				color: '#ffffff',
				font: {fontSize: 18}
			});
			btnShare.add(lblShare);
			var preview = Ti.UI.createImageView({
				left: 0,
				width: scrollPhotos.width - 20,
				height: scrollPhotos.width - 20,
				image: imageFile.toBlob(),
				imageFile: imageFile,
				borderRadius: (scrollPhotos.width - 20)/2,
				btnDelete: btnDelete,
				btnShare: btnShare
			});
			var previewContainer = Ti.UI.createView({
				top: (i == 0) ? 94 : 15,
				left: 0,
				width: scrollPhotos.width,
				height: scrollPhotos.width,
				image: imageFile.toBlob(),
				imageFile: imageFile,
				btnDelete: btnDelete,
				btnShare: btnShare,
				zIndex: 1
			});
			previewContainer.add(preview);
			preview.addEventListener('singletap', function (e) {
				if ( e.source.btnDelete.opacity > 0.0 ) {
					e.source.btnDelete.animate({opacity: 0.0, duration: 250});
					e.source.btnShare.animate({opacity: 0.0, duration: 250});
					e.source.btnDelete.opacity = 0.0;
				} else {
					e.source.btnDelete.animate({opacity: 1.0, duration: 250});
					e.source.btnShare.animate({opacity: 1.0, duration: 250});
					e.source.btnDelete.opacity = 1.0;
				}
			});
			previewContainer.add(btnDelete);
			previewContainer.add(btnShare);
			scrollPhotos.add(previewContainer);
		}
	}
	updatePreviews();
	
	// @TODO favorite function with -> App.Properties.setBool(FOTOFILENAME, true | false)
	var favoritesView = Ti.UI.createView({// @TODO favorite view like photo view
		width: 320,
		height: 302,
		right: -1000,
		zIndex: 1
	});
	contentArea.add(favoritesView);
	
	var imageList = [];
	var rightCenterSmall = 160 - 145/8;
	var bottomCenterSmall = 151 - 145/8;
	var rightCenter = 160 - 145/4;
	var bottomCenter = 151 - 145/4;
	for (var i = 0; i < 4; i++) {
		var image = Ti.UI.createImageView({
			image: '/images/bild'+i+'.png',
			height: 145/4,
			width: 145/4,
			right: rightCenterSmall,
			bottom: bottomCenterSmall,
			opacity: 0.0,
			borderRadius: 145/4,
		});
		favoritesView.add(image);
		imageList.push(image);
	}
	
	var btnFavorites = Ti.UI.createButton({
		center: {x:'50%', y:'50%'},
		backgroundImage: '/images/btnFavoriten.png',
		width: 220/2,
		height: 213/2,
		zIndex: 2,
		expanded: false
	});
	favoritesView.add(btnFavorites);
	btnFavorites.addEventListener('singletap', function () {
		Ti.API.log('click');
		if ( !btnFavorites.expanded ) {
			for (var i = 0; i < imageList.length; i++) {
				var centerParams = [
					{right: rightCenter+100, bottom: bottomCenter},
					{right: rightCenter+60, bottom: bottomCenter-100},
					{right: rightCenter-60, bottom: bottomCenter-100},
					{right: rightCenter-100, bottom: bottomCenter},
				];
				imageList[i].animate({right: centerParams[i].right, bottom: centerParams[i].bottom, height: 145/2, width: 145/2, opacity: 1.0,  duration: 250});
			}
			btnFavorites.expanded = true;
		} else {
			for (var i = 0; i < imageList.length; i++) {
				imageList[i].animate({right: rightCenterSmall, bottom: bottomCenterSmall, height: 145/4, width: 145/4, opacity: 0.0, duration: 250});
			}
			btnFavorites.expanded = false;
		}
	});
	
	//menubar
	var bottomBackground = Ti.UI.createView({
		backgroundColor: '#77767b',
		bottom: 0,
		left: 0,
		width: Ti.UI.FILL,
		height: menuBarHeight
	});
	self.add(bottomBackground);
	var menuBar = Ti.UI.createImageView({
		backgroundImage: '/images/Leiste.png',
		bottom: 0,
		center: {x: '50%'},
		left: 0,
		width: Ti.UI.FILL,
		height: menuBarHeight,
		extended: false,
		zIndex: 999
	});
	
	var menuHeight = 260;
	var duration = 400;
	
	var btnPreStatePhoto;
	var btnPreStateFavorite;
	menuBar.addEventListener('singletap', function () {
		if (menuBar.extended) {
			menuContent.animate({bottom: -1*menuHeight, duration: duration});
			menuBar.animate({bottom: 0, duration: duration});
			menuBar.extended = false;
			btnFavorite.backgroundImage = btnPreStateFavorite;
			btnPhoto.backgroundImage = btnPreStatePhoto;
			searchbar.blur();
		} else {
			menuContent.animate({bottom: 0, duration: duration});
			menuBar.animate({bottom: menuHeight, duration: duration});
			menuBar.extended = true;
			btnPreStatePhoto = btnPhoto.backgroundImage;
			btnPreStateFavorite = btnFavorite.backgroundImage;
			btnFavorite.backgroundImage = btnFavorite.backgroundDeselected;
			btnPhoto.backgroundImage = btnPhoto.backgroundDeselected;
		}
	});
	var menuIcon = Ti.UI.createImageView({
		image: '/images/Menu_Button.png',
		center: {x: '50%', y: '50%'},
		width: 55/2,
		height: 43/2
	});
	menuBar.add(menuIcon);
	var menuContent = Ti.UI.createView({
		height: menuHeight,
		bottom: -1*menuHeight,
		backgroundColor: '#dd77767b'
	});
	// menuContent
	var searchbar = Ti.UI.createSearchBar({
		barColor:'#ffffff', 
	    showCancel:true,
	    height: 43,
	    top: 2,
	    hintText: 'Suche nach...',
	   	backgroundColor: '#88ffffff',
	   	tintColor: '#464f6e'
	});
	searchbar.addEventListener('cancel', function() {
		searchbar.blur();
	});
	menuContent.add(searchbar);
	
	var btnFriends = Ti.UI.createView({
		top: searchbar.height + 30,
		width: Ti.UI.FILL,
		height: 40,
		backgroundColor: '#88ffffff'
	});
	iconFriends = Ti.UI.createImageView({
		image: '/images/Freund_Button.png',
		//left: 0,
		right: 290,
		center: {y:'50%'},
		height: btnFriends.height/2,
		width: 'auto'
	});
	btnFriends.add(iconFriends);
	lblFriends = Ti.UI.createLabel({
		center: {y: '50%'},
		right: 10,
		text: 'Freunde',
		font: {fontSize: '14'},
		color: '#464f6e'
	});
	btnFriends.add(lblFriends);
	menuContent.add(btnFriends);
	
	var btnInterests = Ti.UI.createView({
		top: btnFriends.top + btnFriends.height + 2,
		width: Ti.UI.FILL,
		height: 40,
		backgroundColor: '#88ffffff'
	});
	iconInterests = Ti.UI.createImageView({
		image: '/images/Freund_Button.png',
		//left: 0,
		right: 290,
		center: {y:'50%'},
		height: btnInterests.height/2,
		width: 'auto'
	});
	btnInterests.add(iconInterests);
	lblInterests = Ti.UI.createLabel({
		center: {y: '50%'},
		right: 10,
		text: 'Interessen',
		font: {fontSize: '14'},
		color: '#464f6e'
	});
	btnInterests.add(lblInterests);
	menuContent.add(btnInterests);
	
	self.add(menuContent);
	
	self.add(menuBar);
	
	return self;
}

module.exports = FirstView;
